use torg_firm

/*������ ������ ���� ���� ����������� � ��������  
�� 5 �� 18 � ������� �� ����� �� ����� ��� ���� 23 ��������.*/
select id_tovar, Nazva, Price, NaSklade
	from torg_firm.dbo.tovar AS T
	where Price between 5 and 18 and NaSklade <= 23

/*������ ��������� ��� ���� �� ��������� ���� ���������.*/
select id_zakaz, id_klient, id_sotrud, date_rozm, date_naznach
	from torg_firm.dbo.zakaz AS Z	
	where date_rozm is null

/*������ �볺��� �� ����� adress ������������ �� id � ������� ��������.*/
 select id_klient, Nazva, Adress
	from torg_firm.dbo.klient AS K
	where Adress is not null
order by  id_klient DESC

/*������ ������ ������� ������������� ������� � ���������� �������.*/
select T.id_postav, P.Nazva, T.id_tovar, T.Nazva, T.Price, T.NaSklade
from torg_firm.dbo.tovar T join torg_firm.dbo.postachalnik P on T.id_postav = P.id_postach
where P.Nazva like '����� �����������' and T.NaSklade >20

/*��������� ������� ��� ���������� ������ �� ������� 30 ����, ���� �����������-�� � ���.*/
select sum(T.Price*ZT.Kilkist) as Price
from torg_firm.dbo.tovar T
	join torg_firm.dbo.zakaz_tovar ZT on T.id_tovar = ZT.id_tovar
	join torg_firm.dbo.postachalnik p on t.id_postav = p.id_postach
	join torg_firm.dbo.zakaz z on zt.id_zakaz = z.id_zakaz
	where datediff(day,Z.date_naznach, getdate())<31 and P.Nazva like N'�����%'

/*��������� ���� ����������� �� ����, ���� ����  ������������� ���������� �� ������� �볺���*/
select K.id_klient, K.Nazva, S.id_sotrud, Z.date_naznach, Z.date_rozm
from torg_firm.dbo.sotrudnik S
 join torg_firm.dbo.zakaz Z on S.id_sotrud = Z.id_sotrud
 join torg_firm.dbo.klient K on K.id_klient = Z.id_klient
 where k.Nazva like N'�� ����� �.�.'

 /*������ �볺��� �� � �� � �������� ������ � ��������*/
 select K.id_klient, K.Nazva, Z.date_naznach
 from torg_firm.dbo.klient K
 join torg_firm.dbo.zakaz Z on k.id_klient = Z.id_klient
 where k.Nazva like N'��%' and MONTH(z.date_naznach) = month(dateadd(month,-2,getdate()))

/*������ �����������, ������������ �� �������*/
select s.id_sotrud, s.Fname + ' ' + s.[Name] + ' ' + s.Lname as Sotrudnik
	from torg_firm.dbo.sotrudnik s
order by s.[Name] DESC

/*����� �볺���, �� date_naznach ����� � ����� ����*/
SELECT Z.date_naznach, Z.date_rozm, K.Nazva
FROM torg_firm.dbo.zakaz Z
	join torg_firm.dbo.klient K on Z.id_klient = K.id_klient
	WHERE date_naznach Between '01.11.2017' And '10.12.2020';

/*������ ��������� �� ����� ���� ��� ������ ������.*/
select T.id_tovar, T.Nazva, T.id_postav, T.Price, Z.date_naznach
	from torg_firm.dbo.tovar T join torg_firm.dbo.zakaz_tovar ZT on T.id_tovar = ZT.id_tovar
		join torg_firm.dbo.zakaz Z on ZT.id_zakaz = Z.id_zakaz
where T.Nazva like N'������' and Z.date_naznach < '2020.12.12'

/*������� ��� �������������, �� �������� � ������ �� ������� ���� ���������� �� ����� �*/
SELECT P.id_postach, P.Nazva, P.City, P.Kontakt_osoba
FROM torg_firm.dbo.postachalnik P
WHERE Kontakt_osoba like N'�%' and City like N'�%';

---------------------------------------------------------------------------------------------
/*������ ��������� �� ����� ���� ��� ������ ������.*/
select T.id_tovar, T.Nazva, T.id_postav, T.Price, Z.date_naznach
	from torg_firm.dbo.tovar T join torg_firm.dbo.zakaz_tovar ZT on T.id_tovar = ZT.id_tovar
		join torg_firm.dbo.zakaz Z on ZT.id_zakaz = Z.id_zakaz
where T.Nazva like N'������' and Z.date_naznach < '2018.12.12'

/*������ ������ ���� ���� ����������� � �������� �������� � ������� �� ����� �� �����  �� 15 �������.*/
select T.id_tovar, T.Nazva, T.Price, T.NaSklade
	from torg_firm.dbo.tovar t
	where T.Price between 10 and 20 and T.NaSklade >= 15

/*������ ��������� ��� ���� �� ��������� ���� ���������.*/
select Z.id_zakaz, Z.date_naznach, Z.id_klient
from torg_firm.dbo.zakaz z
where Z.date_naznach is null

/*������ ������ ������� ������������� ������� � ���������� �������.*/
select T.id_postav, p.Nazva, T.id_tovar, T.Nazva, T.Price, T.NaSklade
	from torg_firm.dbo.tovar T join torg_firm.dbo.postachalnik p on T.id_postav = p.id_postach
	where p.Nazva like N'����� �����������' and T.NaSklade > 10

/*��������� ������� ��� ���������� ������ �� ������� 30 ����, ���� �����-�������� � ���.*/
select sum(T.Price*ZT.Kilkist) as Summa
	from torg_firm.dbo.tovar T
		join torg_firm.dbo.zakaz_tovar ZT on T.id_tovar = ZT.id_tovar
		join torg_firm.dbo.postachalnik p on T.id_postav = p.id_postach
		join torg_firm.dbo.zakaz z on ZT.id_zakaz = Z.id_zakaz
	where datediff(day,Z.date_naznach,getdate()) < 31 and p.Nazva like N'���%'

/*��������� ���� ����������� �� ����, ���� ����  ������������� ��������-�� �� ������� �볺���*/
select k.id_klient, k.Nazva, s.id_sotrud, Z.date_naznach, Z.date_rozm
	from torg_firm.dbo.sotrudnik s
		join torg_firm.dbo.zakaz z on s.id_sotrud = Z.id_sotrud
		join torg_firm.dbo.klient k on k.id_klient = Z.id_klient
	where k.Nazva like N'�� ����� �.�.'

/*������ ������������� �� � ��� � �� ��������� ������*/
select p.id_postach, p.Nazva, T.id_tovar, T.Nazva, T.NaSklade
	from torg_firm.dbo.postachalnik p
		join torg_firm.dbo.tovar T on p.id_postach = T.id_postav
where p.Nazva like N'���%' and T.NaSklade is null

/*������ �볺��� �� � �� � �������� ������ � ������������ �����.*/
select k.id_klient, k.Nazva, Z.date_naznach
	from torg_firm.dbo.klient k
		join torg_firm.dbo.zakaz z on k.id_klient = Z.id_klient
where k.Nazva like N'��%' and MONTH(Z.date_naznach) = month(dateadd(month,-1,getdate()))

/*������ �����������, �� ��� ����� ������������ �� �������*/
select s.id_sotrud, s.Fname + ' ' + s.[Name] + ' ' + s.Lname as Sotrudnik
	from torg_firm.dbo.sotrudnik s
order by s.[Name]

/*������ �볺��� �� ����� e-mail ������������ �� id.*/
select K.id_klient, K.Nazva, K.Email
	from torg_firm.dbo.klient K
	where K.Email is not null
order by k.id_klient
