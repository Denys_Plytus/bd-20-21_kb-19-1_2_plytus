create database workshop
use workshop

/*1.	���������� �� ��������� DDL-��������� ���� SQL ���� ����� �� 
������������� ��������. ������ � ������� ������ ������ (�� ����� 3-�)*/

-----------------------��������� ��---------------------------
create table car(
	id_car int identity (100,1) not null primary key, 
	number int not null, 
	FIO_owner nvarchar(50) not null check  (FIO_owner like '[A-z]% [A-z]% [A-z]%'), 
	marka nvarchar(15) not null, 
	year_of_issue int not null 
)

create table auto_mechanic(
	id_workman int identity (200,1) not null primary key, 
	rozrad_workman int not null, 
	FIO_workman nvarchar(50) not null check  (FIO_workman like '[A-z]% [A-z]% [A-z]%'), 
	bonus money not null, 
	personnel_number nvarchar(10) 
)

create table naryad (
	id_naryad int identity (300,1) not null primary key,
	id_workman int foreign key references auto_mechanic(id_workman),
	id_car int foreign key references car(id_car),
	price money not null, 
	real_date_vydachi date not null, 
	planing_date_end date not null, 
	real_date_end date not null, 
	type_of_work nvarchar(50) not null 
)

-----------------------���������� ��---------------------------

Insert into car (number, FIO_owner, marka, year_of_issue) Values
	(5656645, 'Abramchyk Muhailo Volodumurovuch','Volvo', 2019),
	(9845665, 'Lutsevych Oleksandra Oleksandrivna', 'Mersedes', 2020),
	(7823570, 'Matvienko Maryana Vasulivna', 'Bingo', 2008),
	(9856890, 'Petrovna Olga Mykolaivna','Mersedes-600', 2016),
	(4534875, 'Galchin Anastasia Volodumurivna', 'Volvo', 2000),
	(7868788, 'Micenko Anastasia Oleksandivna', 'Mersedes', 2012),
	(1042042, 'Petruk Vadim Anatoliyovych', 'Zhiguli', 1935);

Insert into auto_mechanic (rozrad_workman, FIO_workman, bonus, personnel_number) Values
	(4, 'Connor Joseph Bennett', 450.00, 'AA01-001'),
	(1, 'William Ian Sanchez', 150.00, 'AA01-002'),
	(3, 'Alex Ethan Lewis', 350.00, 'AA01-003'),
	(1, 'Christian Eric Gonzales', 150.00, 'AA01-004'),
	(2, 'Ian Xavier Foster', 250.00, 'AA01-005'),
	(2, 'Jose Caleb Butler', 250.00, 'AA01-006'),
	(3, 'Alexander Antonio Davis', 350.00, 'AA01-007');

Insert into naryad (id_workman, id_car, price, real_date_vydachi, planing_date_end, real_date_end, type_of_work) Values
	(200, 100, 1500.20, '2020-11-11', '2020-11-11', '2020-11-09', 'cleaning'),
	(201, 101, 1500.20, '2020-11-11', '2020-11-11', '2020-11-09', 'cleaning'),
	(202, 103, 750.00, '2020-11-11', '2020-11-11', '2020-11-09', 'kolosa'),
	(203, 102, 1500.20, '2020-11-11', '2020-11-11', '2020-11-09', 'cleaning'),
	(204, 106, 1440.20, '2020-11-11', '2020-11-11', '2020-11-09', 'examination'),
	(205, 100, 100.20, '2020-11-11', '2020-11-11', '2020-11-09', 'examination'),
	(206, 104, 80.20, '2020-11-11', '2020-11-11', '2020-11-09', 'kolosa'),
	(204, 105, 1500.20, '2020-11-11', '2020-11-11', '2020-11-09', 'cleaning'),
	(202, 103, 750.20, '2020-11-11', '2020-11-11', '2020-11-09', 'motor_zloman'),
	(204, 106, 1500.20, '2020-11-11', '2020-11-11', '2020-11-09', 'cleaning');

-----------------------��������� ��� �������---------------------------
	SELECT * from car;
	SELECT * from auto_mechanic;
	SELECT * from naryad;

-------------------------------------������-------------------------------------
/*2.	������ 1 ���������� �� ����� �� ��� SQL*/
--1.	������ ������� ���� ��������, ���� ��������� �������� � ��������� �����������
--���� ������ 2�� ������ ���� � ����� ������� �����
select AM.FIO_workman, C.marka, C.year_of_issue, count(N.id_naryad) as [count_naryadov]
from car C
inner join 
(
	auto_mechanic AM
	Inner join naryad N on AM.id_workman = N.id_workman)
	On C.id_car = N.id_car
	Where (
	(
		(C.year_of_issue)<1941)
	)
Group by AM.FIO_workman, C.marka, C.year_of_issue

/*3.	������ 2 ���������� �� �������������*/
--2.	������ �������, ���� ������ ��������� ����� "��������-600" ����������� ������� ��������� ������
Insert into naryad (id_workman, id_car, price, real_date_vydachi, planing_date_end, real_date_end, type_of_work) Values
	(200, 103, 1500.20, '2020-12-12', '2020-11-11', '2020-11-25', 'cleaning'),
	(201, 103, 7500.00, '2018-11-11', '2018-11-10', '2020-11-18', 'motor has been broken');
SELECT * from naryad;
----
CREATE VIEW zatrumka AS
SELECT N.planing_date_end AS Planing_Date,
	   N.real_date_end AS Real_Date,
	   C.marka AS Marka,
	   AM.FIO_workman AS FIO
FROM naryad N
INNER JOIN car C on C.id_car = N.id_car
INNER JOIN auto_mechanic AM on AM.id_workman = N.id_workman
WHERE planing_date_end < real_date_end 

/*��� ��������� �������������*/
SELECT * FROM zatrumka

/*4.	������ 3 ���������� �� ���������, �� ���������� ��� ���������*/
--4.	��������� ��� �������� ���������, ���� ������ ��������� ���� � ��� �� �������. ������� ������� �������� � ���� ��������� �볺���.

USE workshop
GO
CREATE PROC postiynyy_kliyent AS
begin 
	declare @count_cars int
	Select @count_cars= count(*) from car;

	declare @col_cars  int
	set @col_cars = 100+@count_cars
	PRINT @col_cars

	while(@col_cars > 100)
	begin
		declare @count_of_naryad int
		set @count_of_naryad =
		(
			select count(N.id_naryad) from naryad N 
			where id_car = @col_cars
		)
		declare @count_of_top_car int;
		set @count_of_top_car=
		(
			select top 1 count(N.id_workman)
			from naryad N 
			where  id_car=@col_cars
			GROUP BY N.id_workman
			ORDER BY COUNT(N.id_workman) DESC
		)
		if @count_of_top_car=@count_of_naryad 
			select C.FIO_owner, AM.FIO_workman
			from naryad N join car C on N.id_car=C.id_car
			join auto_mechanic AM on AM.id_workman=N.id_workman
			where C.id_car=@col_cars
		set @col_cars=@col_cars-1
	end
end

select * from naryad
select * from car

/*��� ��������� ���������*/
--DROP PROCEDURE postiynyy_kliyent;

exec postiynyy_kliyent;

/*5.	������ 4 ���������� �� ��������� � �������� �� ��������� �����������*/
--4.	��� ����� ������� ���� ���������, ������� ����� ������� ��������� ������������ �� �� �������� ����

CREATE PROC moreOftenCaremanonWork
    @typeOfWork NVARCHAR(50)
AS
begin
	declare @id int
	set @id = (	select top 1 N.id_workman
	from naryad N join auto_mechanic AM on Am.id_workman = N.id_workman
	where  type_of_work=@typeOfWork
	GROUP BY N.id_workman
	ORDER BY COUNT(N.id_workman) DESC)

	declare @rozr int
	set @rozr = (	select AM.rozrad_workman
	from auto_mechanic AM 
	where  Am.id_workman=@id
	)
	print '������� ����� ���������� ������� �� ������ ���� ' +Convert(VARCHAR, @typeOfWork)+' �� ������ - '+Convert(VARCHAR, @rozr)
end

EXEC moreOftenCaremanonWork 'cleaning'
 
--------
  select * from dbo.naryad where type_of_work = 'cleaning'
  select * from dbo.auto_mechanic where id_workman = 200
-------------------------------------------------------------------------------------------------------
/*6.	���������� ������*/
--
use workshop
GO
Create trigger auto_mechanic_on_UPDATE_INSERT
ON auto_mechanic
AFTER INSERT, UPDATE
AS
UPDATE auto_mechanic
set bonus = bonus+ bonus*0.2
where id_workman >210

/*��� ��������� ��������*/
DROP TRIGGER auto_mechanic_on_UPDATE_INSERT

/*��� ���������� ��������*/
DISABLE TRIGGER auto_mechanic_on_UPDATE_INSERT ON auto_mechanic

/*��� ���������� ��������*/
ENABLE TRIGGER auto_mechanic_on_UPDATE_INSERT ON auto_mechanic


Insert into auto_mechanic (rozrad_workman, FIO_workman, bonus, personnel_number) Values
	(1, 'Miguel Diego Mitchell', 150.00, 'AA01-012'),
	(1, 'Landon Jeremiah Bell', 150.00, 'AA01-013'),
	(2, 'Robert Antonio Lopez', 250.00, 'AA01-014'),
	(3, 'Benjamin Jackson Scott', 350.00, 'AA01-015'),
	(3, 'Colin Nicholas Morgan', 50.00, 'AA01-016');

Select * from auto_mechanic;



select top 5 N.id_workman, N.id_car
	from naryad N join auto_mechanic AM on Am.id_workman = N.id_workman
	where  type_of_work='cleaning'
	GROUP BY N.id_workman, N.id_car
	ORDER BY COUNT(N.id_workman) DESC