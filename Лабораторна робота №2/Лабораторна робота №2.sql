CREATE DATABASE business_bureau
USE business_bureau;

CREATE TABLE viddil
(
  id_viddil int primary key not null identity(100,1),
  name_viddil nvarchar(50) not null constraint name_viddil_format check (name_viddil like '[A-z]%'),
  number_tel char(12) not null check  (number_tel like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
)
/*---------------------------------------------*/
CREATE TABLE pracivnuku
(
  id_pracivnuku int not null identity(200,1) primary key,
  [name] nvarchar(15) not null constraint names_format check ([name] like '[A-z]%'),
  surname nvarchar(20) not null constraint surname check (surname like '[A-z]%'),
  pobatkovi nvarchar(25) not null constraint pobatkovi check (pobatkovi like '[A-z]%'),
  id_viddil int not null foreign key(id_viddil) references Viddil(id_viddil)on delete cascade on update no action
)
/*---------------------------------------------*/
CREATE TABLE nakaz
(
  number_nakaz int not null identity(700,1) primary key,
  id_pracivnuku int not null foreign key(id_pracivnuku) references pracivnuku(id_pracivnuku) on delete cascade on update no action,
  date_vykonananya datetime not null,
  [text] text not null,
  date_nakazu date not null, 
  fact_vyconana tinyint not null  
)  
/*---------------------------------------------*/
CREATE TABLE organization
(
  id_organization int not null identity(600,1) primary key,
  name_org nvarchar(50) not null constraint name_org check (name_org like '[A-z]%'),
  [adress_org] nvarchar(50) not null constraint [adress_org] check ([adress_org] like '[A-z]%, [A-z]%, [A-z]% [a-z]%, [0-9]%'),
  telephone_org char(12) check (telephone_org like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  director nvarchar(50) not null constraint director check (director like '[A-z]%'),
  RRN char(29) not null check (RRN like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  taruf_za_1_hv float not null
)  
/*---------------------------------------------*/
CREATE TABLE vhydna_corr
(
  vhidny_number int not null identity(500,1) primary key,
  id_viddil int not null foreign key(id_viddil) references viddil(id_viddil) on delete cascade on update no action,
  date_otrumanya date not null,
  date_vykonana datetime not null
)
/*---------------------------------------------*/
CREATE TABLE vyhidna_corr
(
  vyhidny_number int not null identity(400, 1) primary key,
  id_organization int not null foreign key(id_organization) references organization(id_organization) on delete cascade on update no action,
  id_viddil int not null foreign key(id_viddil) references viddil(id_viddil) on delete cascade on update no action,
  [date] date not null
)
/*---------------------------------------------*/
 CREATE TABLE telephone_peregov
 (
  id_telephone int not null identity(300, 1) primary key,
  id_organization int not null foreign key(id_organization) references organization(id_organization) on delete cascade on update no action,
  id_viddil int not null foreign key(id_viddil) references viddil(id_viddil) on delete cascade on update no action,
  truvalist_rozmov float not null,
  date_rozmo datetime not null
 )
/*--------------------------------------------------------------------------------------------------*/

INSERT INTO viddil(name_viddil, number_tel)
VALUES
  ('technical_information_departament', 380975171566),
  ('planing_and_economic', 380674978843), 
  ('departament_of_perspective_design', 380976750225),
  ('design_departament', 380961011533),
  ('departament_of_technical_support', 380630777497);
  INSERT INTO pracivnuku( [name], surname, pobatkovi, id_viddil)
VALUES 
  ('Denys', 'Plytus', 'Ivanovych', 101),
  ('Myhailo', 'Pekarev', 'Stepanovych', 102),
  ('Andrii', 'Zatyliuk', 'Oleksandrovych', 103),
  ('Natalia', 'Kovban', 'Sergiivna', 104),
  ('Anastasiia', 'Prohnytskia', 'Romanivna', 100),
  ('Vladyslava', 'Progatilova', 'Mykolaiivna', 101),
  ('Olena', 'Plytus', 'Anatoliivna', 100),
  ('Svitlana', 'Shvydka', 'Volodymyrivna', 103),
  ('Anastasia', 'Nesterchuk', 'Oleksandriivna', 102),
  ('Yulia', 'Menes', 'Antonivna', 104),
  ('Veronika', 'Iesypenko', 'Andriivna', 101),
  ('Yurii', 'Lysogor', 'Ivanovich', 103),
  ('Valerii', 'Homenko', 'Sergiyovych', 100);

INSERT INTO nakaz (date_vykonananya, [text], fact_vyconana, date_nakazu, id_pracivnuku)
VALUES
('20200820', 'nakaz pro zarahuvannya studentiv', 1, '20200810', 202),
('20181110', 'nakaz pro zarahuvannya 1 kursu', 0, '20140130', 208),
('20200615', 'nakaz pro vyhidnui', 0, '20140130', 205),
('20201008', 'nakaz pro vidrahuvannya', 1, '20140130', 208),
('20200325', 'nakaz pro pidvydhchenneya vartosti', 0, '20140130', 212),  
('20191208', 'nakaz pro zarahuvannya studentiv', 1, '20140130', 205);

INSERT INTO organization (name_org, adress_org, telephone_org, director, RRN, taruf_za_1_hv)
VALUES
  ('Apple','Ukraine, Zhytomyr, Barashivska street, 5288','380663657878','Sinkevich','68289805984954907989870357954',0.56),
  ('Amazon','Ukraine, Kharkiv, Ilyinsky street, 2168','380984563028','Ptushkin','54687984032548795057984035465',0.56),
  ('Alphabet','Ukraine, Berdychiv, Malikova street, 3457','380789651258','Kolisnuk','35498790068703210687954687032',0.56),
  ('Berkshire Hathaway','Ukraine, Kiev, Brick street, 7312','380783851289','Peptnuk','54954908498498490540658498409',0.56),
  ('Facebook','Ukraine, Kiev, Pokrovskaya street, 966','380651237865','Hurem','65798792895409849405498705465',0.56);

INSERT INTO vhydna_corr(date_otrumanya, date_vykonana, id_viddil)
VALUES
('20191118', '20191201', 104),
('20200710', '20200711', 101),
('20200618', '20200620', 102),
('20200113', '20200516', 103),
('20191231', '20200101', 101),
('20180713', '20200725', 103),
('19790605', '20161102', 101),
('20200531', '20200625', 104);

INSERT INTO vyhidna_corr([date], id_viddil, id_organization)
VALUES
('20191118', 104, 604), 
('20200710', 101, 603),
('20200620', 102, 601),
('20200516', 103, 600),
('20191231', 101, 602),
('20180713', 103, 603),
('20161102', 101, 600),
('20200531', 104, 603);

INSERT INTO telephone_peregov(truvalist_rozmov, date_rozmo, id_viddil, id_organization)
 VALUES
 ('0.56', '2020-08-25 15:10:59', 104, 601),
 ('0.56', '2020-03-17 17:41:54', 100, 601),
 ('0.56', '2020-01-16 09:13:03', 101, 603),
 ('0.56', '2020-12-14 12:04:00', 102, 600),
 ('0.56', '2020-11-25 09:53:15', 103, 604),
 ('0.56', '2020-03-27 08:30:41', 101, 601),
 ('0.56', '2020-07-17 17:36:36', 100, 604),
 ('0.56', '2020-08-05 15:40:52', 104, 602),
 ('0.56', '2020-09-16 12:01:42', 101, 603),
 ('0.56', '2020-06-21 13:27:31', 102, 600);

/*--------------------------------------------------------------------------------------------------*/
  SELECT * FROM viddil;
  SELECT * FROM pracivnuku;
  SELECT * FROM nakaz;
  SELECT * FROM organization;
  SELECT * FROM vhydna_corr;
  SELECT * FROM vyhidna_corr;
  SELECT * FROM telephone_peregov;