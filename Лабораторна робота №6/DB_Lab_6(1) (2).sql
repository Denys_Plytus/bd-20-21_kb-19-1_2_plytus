use torg_firm

update zakaz_tovar
 set Znigka = (Select case 
					when Kilkist < 5 then 0
					when Kilkist between 5 and 15 then 5
					end 
					from zakaz_tovar)


--1.	��������� ��������� ��� ��������� ���� � ������� ������, �������� ���������� ������������.

CREATE PROC lab7_proc1 AS
SELECT Tovar.Nazva, tovar.Price*zakaz_tovar.Kilkist AS Vartist, sotrudnik.Fname, sotrudnik.Name 
	FROM sotrudnik INNER JOIN zakaz ON sotrudnik.K_sotrud=zakaz.k_sotrud 
	inner join zakaz_tovar on zakaz.K_zakaz=zakaz_tovar.K_zakaz  
	INNER JOIN tovar ON tovar.K_tovar=zakaz_tovar.k_tovar
WHERE sotrudnik.Name='����'

exec lab7_proc1 

-----------------------------------------------
--����������  �� �� �������� ���������, �� �����������:
use reiting

--1.	���������� ���������� ���� �������� �� ����� � ��� ���������� ��������.
alter PROC CentreAVG
AS
BEGIN
SELECT AVG(Reiting.Reiting) AS marks
FROM Reiting
WHERE REiting.K_zapis IN (
SELECT Rozklad_pids.K_zapis FROM Rozklad_pids WHERE Rozklad_pids.Date BETWEEN '2000-01-01' AND '2020-05-11')
END;

EXEC CentreAVG

--2.	���������� ���������� ���� �������� �� ����� � � ���������� ��������.
alter PROC AvgPeriod
AS
BEGIN 
SELECT  dbo_student.Fname, AVG(Reiting.Reiting) as AVGmark  
FROM Reiting 
	join dbo_student on dbo_student.Kod_stud = Reiting.Kod_student and 
	Reiting.K_zapis IN ( SELECT Rozklad_pids.K_zapis FROM Rozklad_pids WHERE Rozklad_pids.Zdacha_type = 'true')
	Group by dbo_student.Fname
END;

EXEC AvgPeriod

--3.	���������� ��������, �� ���������� �� 4 �� 5.
alter PROC FindGoodStudents
AS
BEGIN
SELECT Reiting.Kod_student
FROM Reiting
WHERE Reiting.Kod_student IN (
SELECT Kod_student FROM (
SELECT AVG(Reiting.Reiting) AS AvgM, Reiting.Kod_student FROM Reiting GROUP BY Reiting.Kod_student HAVING AVG(Reiting.Reiting) BETWEEN '74' AND '100') AS Kod_student)
END;

EXEC FindGoodStudents

--4.��������� �������� ���� ���� �� �� �������� � ������������ ������ �� ECTS.

alter PROC OpenMarks
AS
BEGIN 
Select  dbo_student.Name_ini, sum(Reiting.Reiting) as mark, case 
															WHEN sum(Reiting.Reiting) > 90 THEN '������'
															WHEN sum(Reiting.Reiting) BETWEEN 85 AND 89 THEN '�����'
															WHEN sum(Reiting.Reiting) BETWEEN 75 AND 84 THEN '�����'
															WHEN sum(Reiting.Reiting) BETWEEN 65 AND 74 THEN '���������'
															WHEN sum(Reiting.Reiting) BETWEEN 60 AND 64 THEN '���������'
															when sum(Reiting.Reiting) < 60 then '�����������' end  as 'national marks',

															case 
															WHEN sum(Reiting.Reiting) > 90 THEN 'A'
															WHEN sum(Reiting.Reiting) BETWEEN 85 AND 89 THEN 'B'
															WHEN sum(Reiting.Reiting) BETWEEN 75 AND 84 THEN 'C'
															WHEN sum(Reiting.Reiting) BETWEEN 65 AND 74 THEN 'D'
															WHEN sum(Reiting.Reiting) BETWEEN 60 AND 64 THEN 'F'
															when sum(Reiting.Reiting) < 60 then 'FX' end  as 'ECTS',
												
															 predmet.Nazva
	from Reiting
		join dbo_student on 
			reiting.Kod_student = dbo_student.Kod_stud 
		join Rozklad_pids on
			Rozklad_pids.K_zapis = reiting.K_zapis
		join Predmet_plan on
			Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		join predmet on
			predmet.K_predmet = Predmet_plan.K_predmet
	group by  dbo_student.Name_ini, predmet.Nazva, dbo_student.Fname 
END;
 
EXEC OpenMarks

--5.	 ������ �� ������� ����� � ������� ������� � ���� ��� ����� ����� � ������� �������� �����.
alter TRIGGER InsertStudent ON dbo_student INSTEAD OF INSERT AS
		BEGIN
		DECLARE @group VARCHAR(7)
		SELECT @group = Kod_group FROM inserted
		IF NOT (@group IN (SELECT dbo_groups.Kod_group FROM dbo_groups))
		INSERT INTO dbo_groups(Kod_group, K_navch_plan ) VALUES (@group, 1);
		INSERT INTO dbo_student (Sname, Name, Fname, N_ingroup, Kod_group) 
		SELECT Sname, Name, Fname, N_ingroup, Kod_group FROM inserted
		END

INSERT INTO dbo_student(Sname, Name, Fname, N_ingroup, Kod_group)
VALUES('�������', '������', '��������������', '3', 'ϲ-33') 


select * from dbo_student



select * from dbo_groups
--6.	������ �� ����������� ����� � �������  �������� ���� ����� ���� �������� � ���� ����� ���������. 
	ALTER TRIGGER DeleteStudents ON dbo_student FOR DELETE AS
		BEGIN
		DECLARE @group varchar(8)
		SELECT @group = Kod_group FROM deleted
		IF NOT EXISTS(SELECT dbo_student.Kod_group FROM dbo_student WHERE dbo_student.Kod_group = @group)
		BEGIN
		DELETE FROM dbo_groups WHERE dbo_groups.Kod_group = @group
		END
	END
DELETE FROM dbo_student WHERE dbo_student.Sname LIKE '�������'


