use business_bureau
/*-------------------------------------procedures-------------------------------------*/
/*0.	������� ���������*/
GO
Create procedure summ as
begin
select name as Name, surname, pobatkovi
from pracivnuku P
end;

Exec summ

Drop Procedure summ
-------------------
/*1.	�������� ��������� ��� ��������� ���� ������ 
�� 1 �� � ��������, �� ��������� �������������� � 2019-11-18 �� 2020-05-10*/
GO
Create procedure changePrice as
begin
Update organization set  taruf_za_1_hv= taruf_za_1_hv*0.5
WHERE name_org=(select vyhidna_corr.[date]
from vyhidna_corr 
where [date] between '2019-11-18' and '2020-05-10')
end

/*�������� ������ � ��������� changePrice*/
Exec changePrice

--�������� ��������� changePrice
Drop Procedure changePrice
-------------------
/*2.	�������� ��������� ��� ���� ������� 
���������� � ������ �������� �������� �� ��������� ��������*/
Go
CREATE PROC  Change_surname
@a nvarchar(15), @i INT
as
update pracivnuku set surname = @a
where id_viddil=@i

/*�������� ������ � ��������� Change_surname*/
exec Change_surname @a='Polishchuk', @i=10

/*�������� ��������� change_rrn*/
Drop Procedure Change_surname
-------------------
/*3.	�������� ��������� ��� ���� �������������� ������� �����������, 
�� ����� ���������� �� �������� ������ �����������*/
GO
Create proc change_rrn
@name nvarchar(50), @rrn nvarchar(29)
as
begin
Update organization set RRN= @rrn
where name_org= @name
end

/*�������� ������ � ��������� change_rrn*/
exec change_rrn 'Apple', '67548979430250546707551543461'

/*�������� ��������� change_rrn*/
Drop Procedure change_rrn
-------------------

/*-------------------------------------trigers-------------------------------------*/
/*1.	������, �� ��������� ��� ��������� �� ��������� �����
(����� ������ �� �� 1 �� �� 20%)*/

GO
Create trigger Organization_Update
ON organization
After Insert, Update
AS
UPdate organization
SET taruf_za_1_hv = taruf_za_1_hv * 0.2
WHere id_organization = (Select id_organization From inserted)

/*���������� ������� Organization_Update*/
DISABLE TRIGGER Organization_Update On Organization

/*��������� ������� Organization_Update*/
Enable Trigger Organization_Update On Organization

/*�������� ������*/
DROP TRIGGER Organization_Update

/*���������� �������*/
Insert into organization( name_org, adress_org, telephone_org, director, RRN, taruf_za_1_hv)
Values 
('Coca-Cola', 'Ukraine, Berdychiv, Br street, 1a', '380789654123',
'Opanacuk', '19384756783104985731475647348', 0.9)
SELECT * FROM organization	
-------------------																							
/*2.	������, �� ��������� ��� �������� ����������
� ������������� ������� deleted*/

/*�������� ������� ��� ��������� ������*/
Create table deleted_prac
(
	Id INT IDENTITY PRIMARY KEY,
    name NVARCHAR(15) NOT NULL,
    surname NVARCHAR(15) NOT NULL,
    pobatkovi NVARCHAR(15) NOT NULL,
);
-----
Go
Create trigger pracivnuku_delete
on pracivnuku
after delete
as
insert into deleted_prac (name, surname, pobatkovi)
select name, surname, pobatkovi
from deleted

/*�������� ������*/
DROP TRIGGER pracivnuku_delete

/*���������� �������*/
Delete from pracivnuku
where id_pracivnuku =212
Select * from deleted_prac

-------------------
/*3.	������, �� ��������� ��� �������� ����������
� ������������� ������� deleted*/
GO
Create trigger pracivnuku_update
on pracivnuku
after update
as
insert into deleted_prac (Id, name, surname, pobatkovi)
select  id_pracivnuku, 'update ' + name,  surname, pobatkovi
from inserted

/*���������� ������� Organization_Update*/
DISABLE trigger Organization_Update On Organization

/*��������� ������� Organization_Update*/
Enable Trigger Organization_Update On Organization

/*�������� ������*/
DROP TRIGGER pracivnuku_update

/*���������� �������*/
insert into pracivnuku (id_pracivnuku, name, surname, pobatkovi)
Values (213, 'Oleg', 'Korniuchuk', 'Mykolayovuch');

update pracivnuku set surname = 'Korn'
where surname = 'Korniuchuk';

select*from deleted_prac
 
-------------------
/*4.	������, �� ��������� ��� �������� ����������
� ������������� ������� deleted*/