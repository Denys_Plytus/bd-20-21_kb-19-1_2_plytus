use business_bureau

/*������� ������ ��� ��������� � �� �������*/
SELECT sysobjects.name AS �������, sysindexes.name AS ������, sysindexes.indid AS �����
FROM sysobjects 
INNER JOIN sysindexes ON sysobjects.id = sysindexes.id
WHERE (sysobjects.xtype = 'U') AND (sysindexes.indid > 0)
ORDER BY sysobjects.name, sysindexes.indid

/*������� ���������������� ������ ��� ������� 
name ������� pracivnuku, �� ����������� 
�������� ����� ������� ������� �� 50%:*/
create nonclustered index N_prac_name on pracivnuku(name)
with  fillfactor  = 50
select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'pracivnuku')

--drop index N_prac_name on pracivnuku

/*������� ����������� ��������� ������ 
��� �������� name � surname ������� pracivnuku*/
drop index name_surname on pracivnuku
create nonclustered index name_surname on pracivnuku(name, surname)

select OBJECT_NAME(object_id) as table_name,
name as index_name, type, type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID(N'pracivnuku')

/*create table test_index(
id int not null,
pole1 char(36) not null,
pole2 char(216) not null
)

drop table test_index;
declare @i as int=31
while @i<240
begin
	set @i = @i+1;
	insert into test_index
		values(@i, 'a','b')
	end;

	insert into test_index
values(241, 'a','b')


select index_type_desc,  page_count,
record_count, avg_page_space_used_in_percent
from sys.dm_db_index_physical_stats
(db_id(N'test_index'), OBJECT_ID(N'test_index'), Null,
 Null, 'Detailed')
 */

 select index_type_desc,  page_count,
record_count, avg_page_space_used_in_percent, avg_fragment_size_in_pages
from sys.dm_db_index_physical_stats
(db_id(N'pracivnuku'), OBJECT_ID(N'pracivnuku'), Null,
 Null, 'Detailed')
      