﻿use reir_dekanat
--1.	Сумарний рейтинг студента з кожної дисципліни.
select dbo_student.Name_ini, predmet.Nazva, sum(reiting.Reiting) as [sum]
from reiting
join dbo_student on dbo_student.Kod_stud = reiting.Kod_student
join dbo_groups on dbo_groups.Kod_group = dbo_student.Kod_group
join Predmet_plan on Predmet_plan.K_navch_plan = dbo_groups.K_navch_plan
join predmet on predmet.K_predmet = Predmet_plan.K_predmet
group by dbo_student.Name_ini, predmet.Nazva
order by dbo_student.Name_ini
	
--2.	Розрахувати кількість студентів у кожній групі. 
select Kod_group, kilk
	from dbo_groups
 
--3.	Розрахувати кількість дисциплін за групою. 
select g.Kod_group, count(K_predmet) as [kilkist_predmetov]
	from (select pp.K_predmet, pp.K_predm_pl
		from Predmet_plan pp) pp
			join (select rp.K_predm_pl, rp.Kod_group from Rozklad_pids rp) rp
				on rp.K_predm_pl = pp.K_predm_pl
join (select g.Kod_group, g.Kod_men, g.K_navch_plan from dbo_groups g) g
					on g.Kod_group = rp.Kod_group
					join Navch_plan n on n.K_navch_plan = g.K_navch_plan
	group by g.Kod_group
	
--4.	Розрахувати кількість проведених занять у кожній групі. 
select dbo_groups.Kod_group, SUM(dbo.Predmet_plan.Chas_all) as [кількість проведених занять]
from  Navch_plan join dbo_groups on Navch_plan.K_navch_plan = dbo_groups.K_navch_plan
		join Predmet_plan on Predmet_plan.K_navch_plan = Navch_plan.K_navch_plan
Group by dbo_groups.Kod_group
	
--5.	Розрахувати середній бал за групою. 
select dbo_groups.Kod_group, AVG(Reiting.Reiting) as [seredniy_bal]
from dbo_groups inner join Rozklad_pids On dbo_groups.Kod_group = Rozklad_pids.Kod_group
	inner join  Reiting On Rozklad_pids.K_zapis = Reiting.K_zapis
	Group by dbo_groups.Kod_group
	

--6.	Розрахувати середній бал з дисципліни. 
select predmet.Nazva, avg(Reiting) as [seredniy_bal_s_desciplini]
from predmet inner join Predmet_plan On predmet.K_predmet = Pred-met_plan.K_predmet
	inner join Rozklad_pids On Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
	inner join Reiting On Rozklad_pids.K_zapis = Reiting.K_zapis
	Group by predmet.Nazva
	

--7.	Розрахувати поточний рейтинг студента з кожної дисципліни. 
select dbo_student.Name_ini, predmet.Nazva, reiting.Reiting
from reiting
join dbo_student on dbo_student.Kod_stud = reiting.Kod_student
join dbo_groups on dbo_groups.Kod_group = dbo_student.Kod_group
join Predmet_plan on Predmet_plan.K_navch_plan = dbo_groups.K_navch_plan
join predmet on predmet.K_predmet = Predmet_plan.K_predmet
where dbo_student.Name_ini like N'Шпаківський%'
order by dbo_student.Name_ini;
	
--1.	Відобразити найменший рейтинг студентів з дисципліни. 
select dbo_student.Name_ini, dbo_groups.Kod_group, predmet.Nazva, MIN(reiting.Reiting) as [MIN]
from reiting
join dbo_student on dbo_student.Kod_stud = reiting.Kod_student
join dbo_groups on dbo_groups.Kod_group = dbo_student.Kod_group
join Predmet_plan on Predmet_plan.K_navch_plan = dbo_groups.K_navch_plan
join predmet on predmet.K_predmet = Predmet_plan.K_predmet
group by dbo_student.Name_ini,predmet.Nazva, dbo_groups.Kod_group
order by dbo_student.Name_ini
	
--2.	Відобразити найбільший студентський рейтинг з дисципліни. 
select dbo_student.Name_ini, dbo_groups.Kod_group, predmet.Nazva, MAX(reiting.Reiting) as [MAX]
from reiting
join dbo_student on dbo_student.Kod_stud = reiting.Kod_student
join dbo_groups on dbo_groups.Kod_group = dbo_student.Kod_group
join Predmet_plan on Predmet_plan.K_navch_plan = dbo_groups.K_navch_plan
join predmet on predmet.K_predmet = Predmet_plan.K_predmet
group by dbo_student.Name_ini,predmet.Nazva, dbo_groups.Kod_group
order by dbo_student.Name_ini
	
--3.	Розрахувати кількість проведених занять за видами для кожної дисципліни. 
Select predmet.Nazva, Count(Predmet_plan.Chas_Lek) as [К-сть лекцій], Count(Predmet_plan.Cahs_pr) as [К-сть практичних], Count(Predmet_plan.Cahs_sam) as [К-сть самостіних], Count(Predmet_plan.Chas_Labor) as [К-сть лабораторних], COUNT(Predmet_plan.Chas_Lek) as [К-сть лекцій], Count(Predmet_plan.Chas_sem) as [К-сть семестрових] 
From predmet
join Predmet_plan on predmet.K_predmet=Predmet_plan.K_predmet
group by predmet.Nazva
	
 

--4.	Розрахувати кількість груп за кожною спеціальністю. 
select s.Nazva, s.Kvalif, count(g.Kod_group) as kilk
	from (select g.Kod_group, g.K_navch_plan from dbo_groups g) g
		join (select np.K_navch_plan, np.K_spets from Navch_plan np) np
		on np.K_navch_plan = g.K_navch_plan
			join Spetsialnost s on s.K_spets = np.K_spets
	group by s.Nazva, s.Kvalif
	
 

--5.	Запит на знищення даних з таблиці «Reiting» за визначеним ко-дом студента (в поле параметра вводиться прізвище студента). 
declare @kod int = 15
delete from reiting
	where kod_student = @kod
--6.	 Запит на оновлення даних у таблиці «Reiting» – передбачити збільшення балів за модульні контролі на 15%. 
	update reiting set reiting = (Reiting * 0.85)
--7.	 Запит на оновлення даних в таблиці «Reiting»– передбачити зменшення балів за іспит на 15%.
update reiting set reiting = (Reiting / 0.85)
--8.	Запит на вставку даних до таблиці «Reiting» – передбачити вста-вку даних студентів визначеної групи (код пари та початковий бал задається динамічно).
declare @kodg nvarchar(10) = 'ПІ-53', @poch_bal int = 60, @kod_zapus int = 17, @prusytnist bit = 0
insert into Reiting (Reiting.K_zapis, Reiting.Kod_student, Reiting.Reiting, Reit-ing.Prisutn)
SELECT @kod_zapus, dbo_student.Kod_stud, @poch_bal, @prusytnist
FROM dbo_student 
WHERE dbo_student.Kod_group = @kodg
--9.	Запит на оновлення даних – передбачити зміну порядкового но-мера змістовного модуля за певною дисципліною на нове зна-чення.
declare @module int = 4
update Predmet_plan set Predmet_plan.Kilk_modul = @module
	from Predmet_plan join predmet on predmet.K_predmet = Predmet_plan.K_predmet
--10.	Передбачити знищення студентів з таблиці «Students» за визна-ченим номером групи.
delete dbo_student from dbo_student
where dbo_student.Kod_group = 'ПІ-54'
--11.	Запит на вставку даних до таблиці «Reiting» – передбачити вста-вку даних студентів визначеної групи (код пари та присутність задаються динамічно) 
declare @kodg1 nvarchar(10) = 'ПІ-53', @poch_bal1 int = 60, @kod_zapus1 int = 17, @prusytnist1 bit = 0
insert into Reiting (Reiting.K_zapis, Reiting.Kod_student, Reiting.Reiting, Reit-ing.Prisutn)
SELECT @kod_zapus1, dbo_student.Kod_stud, @poch_bal1, @prusytnist1
FROM dbo_student 
WHERE dbo_student.Kod_group = @kodg1
--12.	Передбачити оновлення даних у таблиці «Reiting» – поле «Prisutnist», для студента із визначеним прізвищем автоматично встановлюється true.
declare @stud nvarchar(10) = 'Шпаківський'
update reiting set Prisutn = 'true'
	from dbo_student join Reiting on dbo_student.Kod_stud=Reiting.Kod_student
	where dbo_student.Sname = @stud
