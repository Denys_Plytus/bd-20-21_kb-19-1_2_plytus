use business_bureau
----
/*�� ���������� �� ������������ Amazon �� Facebook*/
select TP.truvalist_rozmov, TP.id_telephone, O.name_org, V.name_viddil
from telephone_peregov TP
join organization O on TP.id_organization = O.id_organization
join viddil V on TP.id_viddil = V.id_viddil 
WHERE O.name_org in ('Amazon', 'Facebook');

---------------------------------------------------
---------������� pracivnuku
INSERT INTO pracivnuku(id_pracivnuku, surname, pobatkovi, id_viddil) values 
(213, 'Matvienko', 'Nastya', 'Genadivna', 103);
--------���������
update pracivnuku  set id_pracivnuku=213, surname = 'Matvienko', pobatkovi='Nastya', id_viddil=103
where pobatkovi = 'Oleksandrivna'
--------���������
update pracivnuku SET pobatkovi='Oleksandrivna'

---------------------------------------------------
---------������� organization
INSERT INTO organization(id_organization,name_org, adress_org, telephone_org, director, RRN, taruf_za_1_hv) values 
(605, 'Google', 'Ukraine, Zhytomyr, Barashivska street, 5288', 3807893657845, 'Petrenko',  54687984032547795057964035465, 0.15);
--------���������
update organization set id_organization=605, name_org='Google', 
adress_org='Ukraine, Zhytomyr, Barashivska street, 5288', telephone_org =3807893657845, 
director='Petrenko', RRN=54687984032547795057964035465, taruf_za_1_hv= 0.15
where name_org='Google'
---------------------------------------------------
---------������� nakaz
INSERT INTO nakaz(number_nakaz, id_pracivnuku, date_vykonananya, 
[text], date_nakazu, fact_vyconana) 
values 
(706, 201,'2020-08-20 00:00:00.000', 'nakaz pro zarahuvannya 2 kursu', 
'Petrenko',  '2020-08-10', 1);
--------���������
update nakaz set number_nakaz=706, id_pracivnuku=201, 
date_vykonananya='2020-08-20 00:00:00.000', 
[text] ='nakaz pro zarahuvannya 2 kursu', 
date_nakazu='2020-08-10', fact_vyconana=1
where number_nakaz<=706

---------������� viddil
INSERT INTO viddil(id_viddil, name_viddil, [number_tel]) 
values 
(105,'vyddil_kibersecurity', 380955647856);
--------���������
update viddil set id_viddil=706, name_viddil='vyddil_kibersecurity', 
number_tel=380955647856
where id_viddil<105

---------������� vhydna_corr
INSERT INTO vhydna_corr([vhidny_number],[id_viddil], [date_otrumanya],[date_vykonana]) 
values 
(508,104,1979-06-05,'2020-07-11 00:00:00.000');
--------���������
update vhydna_corr set [vhidny_number]=508, [id_viddil]=104, 
[date_otrumanya]=1979-06-05, [date_vykonana]='2020-07-11 00:00:00.000'
where id_viddil<105

/*������ ��� �����������, �� ���� ���������� ����� �� ����� �������������� � Ukraine*/
select O.name_org, adress_org, TP.date_rozmo
from organization O 
join telephone_peregov TP on TP.id_organization = O.id_organization
where TP.date_rozmo between '2020-01-06' and '2020-09-01' 
and O.adress_org like '%Ukraine%'

/*������� �������������� �� ������ ��������*/
select O.name_org, VYC.[date], V.name_viddil
from vyhidna_corr VYC
join viddil V on V.id_viddil = VYC.id_viddil
join organization O on VYC.id_organization = O.id_organization
where [date] between '2018-01-01' and '2020-01-01'
order by name_org DESC

/*������ ������������, ����� ���� ���������� �� � �� ��������� ������ ���� ���� ����� �� �� �������*/
select V.name_viddil, TP.truvalist_rozmov,TP.date_rozmo, O.name_org
from telephone_peregov TP
join viddil V on V.id_viddil = TP.id_viddil
join organization O on O.id_organization = TP.id_organization
where truvalist_rozmov >= 0.30 and name_org like N'A%'

/*ϳ������� ����� �� ����������� � ��� 602 �� 13%.*/
update organization set taruf_za_1_hv = (1.13*taruf_za_1_hv)
where id_organization=602

/*�������� ��� ���������� � � �������� "������"*/
DELETE* from pracivnuku where surname='Kovban'

/*������ �����, ����� ���� ���������� � departament*/
declare @name nvarchar(50)='departament'
select V.id_viddil, V.name_viddil, V.name_viddil, V.number_tel
from viddil V
where name_viddil like @name

/*������ ��������������, ��� ������� � �������� ���� �� ����� � id=101*/
declare @id int =101
select VHC.vhidny_number, VHC.id_viddil, VHC.date_otrumanya, VHC.date_vykonana
from vhydna_corr VHC
where MONTH(VHC.date_otrumanya) = month(DATEADD(year,-1, getdate()))
and id_viddil = @id;

/*������ �� ���� �������� �� �����������*/
declare @fv int =1
select N.id_pracivnuku,N.date_vykonananya, N.[text], N.date_nakazu, N.fact_vyconana 
from nakaz N
where fact_vyconana = @fv
order by date_nakazu

/*�� ������ �� ������� ����������*/
declare @prac int = 208
select  P.name, N.date_vykonananya, N.[text]
 from nakaz N
 join pracivnuku P on N.id_pracivnuku = P.id_pracivnuku
 where P.id_pracivnuku = @prac

 /*������ ��� �����������, �� ���� ���������� ����� �� ����� �������������� � ���*/
 declare @date1 nvarchar(10) = '2020-01-06'
 declare @date2 nvarchar(10) = '2020-09-01' 
 select O.name_org, adress_org, TP.date_rozmo
 from organization O 
 join telephone_peregov TP on TP.id_organization = O.id_organization
 where TP.date_rozmo between @date1 and @date2
 and O.adress_org like '%Kiev%'

 /*������� �������������� �� ������ �������� �� ��� ����*/
 declare @date_1 nvarchar(10) = '2018-01-01'
 declare @date_2 nvarchar(10) = '2020-01-01'
 select O.name_org, VYC.[date], V.name_viddil
 from vyhidna_corr VYC
 join viddil V on V.id_viddil = VYC.id_viddil
 join organization O on VYC.id_organization = O.id_organization
 where [date] between @date_1 and @date_2
 order by name_org DESC

 /*������ ������������, ����� ���� ���������� �� � �� 
 ��������� ������ ���� ���� ����� �� �� �������*/
 declare @truv float = 0.30
 select V.name_viddil, TP.truvalist_rozmov,TP.date_rozmo, O.name_org
from telephone_peregov TP
join viddil V on V.id_viddil = TP.id_viddil
join organization O on O.id_organization = TP.id_organization
where truvalist_rozmov >= @truv and name_org like N'A%'
