use torg_firm

--1.	������� �������� ������� ������ �� ���������.
select sum(tovar.NaSklade) as sumNaSklade
	from tovar

--2.	������� �������� ������� ����������� ����������. 
select count(id_sotrud) as sumSotr
	from sotrudnik

--3.	������� �������� ������� ������������� ����������. 
select count(id_postach) as sumPostach
	from postachalnik

--4.	������� ������� �� ������ �������, �� �������� � ��������� �����. 
select count(id_postach) as sumPostach
select tovar.id_tovar, tovar.Nazva, sum(zakaz_tovar.Kilkist) as Kilkist
	from tovar inner join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar
	where zakaz_tovar.id_zakaz in (
		select zakaz.id_zakaz from zakaz
			where zakaz.date_naznach between '2020-11-01' and '2020-11-30')
	group by tovar.Nazva, tovar.id_tovar

--5.	������� ����, �� ��� ���� �������� ������ � ��������� �����. 
select sum(qresult.zag_vartist) as [Sum]
	from qresult
	where qresult.id_zakaz in(
		select zakaz.id_zakaz from zakaz
			where zakaz.date_naznach between '2020-11-01' and '2020-11-30')

--6.	������� ���� ������� ������ �� ������ ��������������.
select postachalnik.Nazva, sum(qresult.zag_vartist) as [Sum]
	from (postachalnik inner join tovar on postachalnik.id_postach = to-var.id_postav)
		inner join qresult on qresult.id_tovar = tovar.id_tovar
	group by postachalnik.Nazva

--7.	������� �������� ������� ��������� �� ������ �����������-���, �� ����� ������. 
select postachalnik.Nazva, count(zakaz_tovar.id_zakaz) as Kilkist
	from (zakaz_tovar inner join tovar on zakaz_tovar.id_tovar = tovar.id_tovar)
		inner join postachalnik on postachalnik.id_postach = tovar.id_postav
	where tovar.Nazva = '������'
	group by postachalnik.Nazva

--8.	������� ������� ����, �� ��� ���������� �����.
select tovar.Nazva, avg(qresult.zag_vartist) as avgTovar
	from qresult inner join tovar on qresult.id_tovar = tovar.id_tovar
	group by tovar.Nazva

--9.	������� ������� ��������� ��� �볺���, �� �������� � ����-���. 
select sum(qresult2.ltog) as [Sum]
	from qresult2
	where qresult2.id_zakaz in (
		select id_zakaz from zakaz
			where id_klient in (
				select id_klient from klient
					where klient.City like '�������'))

--10.	������� ������� ���� �� ������ �� ������� �������������.
select postachalnik.Nazva, avg(tovar.Price) as [avg]
	from tovar inner join postachalnik on postachalnik.id_postach = to-var.id_postav
	group by postachalnik.Nazva
